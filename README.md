# Information Security posters

Some Information Security posters, an **entry-level** recap of tools
and best-practices.

## Build

On a GNU/Linux system where XeLaTeX is installed, type `make` on the
command line to build a new version of the PDF file.
