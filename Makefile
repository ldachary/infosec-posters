project.pdf: project.tex img/
	xelatex project.tex && xelatex project.tex

clean:
	rm -fr project.aux project.log project.toc project.bbl project.bcf \
	       project.blg project.out project.run.xml *~
