Copyright (c) 2017 Fabio Natali

This work is licensed under a Creative Commons Attribution 4.0
International License. See
https://creativecommons.org/licenses/by/4.0/.

Exception: `img/password_strength_xkcd.png` by XKCD, see
https://xkcd.com/936/ for details.
